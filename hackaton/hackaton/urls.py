from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hackaton.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^app/', 'application.views.test_view', name='test_view'),
    url(r'^api/get/', 'application.views.get_view', name='get_view'),
    url(r'^api/save', 'application.views.post_view', name='post_view')
)
