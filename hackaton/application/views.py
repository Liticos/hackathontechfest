from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from PostIt import *
from mongo_connector import Requester
import json
from bson import json_util
import TweetPoster
from django.views.decorators.http import require_http_methods
from django.core.urlresolvers import reverse

# Create your views here.

def test_view(request):
	return render_to_response('index.html', { 'data': 'info de prueba' })


def get_view(request):
	r = Requester()
	days_ = request.GET.get("days")
	if days_:
		days_ = int(days_)
	it = None
	et = None
	if et and et > 0:
		it = datetime.datetime.utcnow()
		et = it - datetime.timedelta(days=days_)
   	tags = request.GET.get("tag")
	if tags:
		return HttpResponse(json.dumps(r.getDoc((it, et), tags), default=json_util.default), content_type="application/json")
	return HttpResponse(json.dumps(r.getDoc((None,None)), default=json_util.default), content_type="application/json")

def post_view(request):
	title = request.POST.get("title")
	text = request.POST.get("text")
	tags = request.POST.get("tags").split(',')
	r = Requester()
	#t = TweetPoster()
	r.postDoct(title, text, *tags)
	#t.post(title, text, *tags)
	return HttpResponseRedirect(reverse('test_view',None))
