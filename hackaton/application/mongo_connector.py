__author__ = 'Netwave'

from pymongo import MongoClient
from PostIt import *




class Requester():
    def __init__(self):
        self.client = MongoClient('mongodb://admin:admin@ds037601.mongolab.com:37601/hackathon')
        self.db = self.client.hackathon
        self.collection = self.db.socialboard

    def getDoc(self, timeRange, *tags):
        initT, endT = timeRange
        toSearch = {}
        if initT or endT:
            toSearch["date"] = {}
        if initT != None:
            toSearch["date"]["$lt"] = initT
        if endT != None:
            toSearch["date"]["$gt"] = endT
        taglst = list(tags)
        if taglst != []:
            toSearch["tags"] = {"$in": list(tags)}
        reslst = [p for p in self.collection.find(toSearch, {"_id": False}).sort("date")]
        reslst.reverse()
        return reslst

    def postDoct(self, title, text, *tags):
        post = PostIt(title, text, *tags)
        self.collection.insert(post)

    def __del__(self):
        self.client.close()


if __name__ == "__main__":
    r = Requester()
    ##for i in range(15):
    ##    r.postDoct("test"+str(i), "test de posting a db", "prueba", "test")

    it = datetime.datetime.utcnow()
    et = it - datetime.timedelta(hours=5)
    data = r.getDoc((None,None))