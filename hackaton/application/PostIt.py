__author__ = 'Netwave'

import datetime
import json
from bson import json_util

class PostIt(dict):
    def __init__(self, title, text, *tags):
        dict.__init__(self)
        self["title"] = title
        self["text"] = text
        self["tags"] = list(tags)
        self["date"] = datetime.datetime.utcnow()

    def jsonize(self):
        return json.dumps(self, default=json_util.default)


lst = []
for i in range(10):
    lst.append(PostIt("test"+str(i), "testestestes", "test", "Prueba"))

